<?php

namespace Meiji\BxPhinx\Migration;

class Manager
{
	
	private static $arConfiguredCommands = [
		'breakpoint',
		'create',
		'migrate',
		'rollback',
		'status',
		'test',
		'seed:run',
		'seed:create'
	];
	
	private static $arEnvCommands = [
		'breakpoint',
		'migrate',
		'rollback',
		'status',
		'test',
		'seed:run',
	];
	
	private static $arDisabledCommands = [
		'init'
	];
	
	static public function defineArgv($args)
	{
		
		if (isset($args[1])) {
			
			$command = $args[1];
			
			if (in_array($command, self::$arDisabledCommands)) {
				exit('command disabled' . PHP_EOL);
			}
			
			$configOpt = !in_array($command, self::$arConfiguredCommands) ? [] :
				['--configuration', __DIR__ . '/../../config/app.inc'];
			
			$envOpt = !in_array($command, self::$arEnvCommands) ? [] : ['--environment', 'default'];
			
			$parseOpt = ($command == 'list') ? [] : ['--parser', 'php'];
			
			$templateOpt = !($command == 'create') ? [] :
				['--template', __DIR__ . '/../../templates/BitrixBase.template.php.dist'];
			
			$iteractionOpt = !($command == 'migrate') ? [] :
				['--no-interaction'];
			
			$args = array_merge($args, $configOpt, $envOpt, $parseOpt, $templateOpt, $iteractionOpt);
			
		}
		
		
		return $args;
	}
}