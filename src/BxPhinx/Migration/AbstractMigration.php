<?php

namespace Meiji\BxPhinx\Migration;

use Bitrix\Main\Loader;


/**
 * Абстракция Битрикс-миграции
 *
 * @author Artem Iksanov <iksanov@meiji.media>
 */

abstract class AbstractMigration extends \Phinx\Migration\AbstractMigration
{
	
	/**
	 * @var \CMain
	 */
	protected $APP;
	
	/**
	 * @var \CDatabase
	 */
	protected $DB;
	
	/**
	 * @var \CUser
	 */
	protected $USER;
	
	/**
	 * Инициализация скрипта.
	 */
	protected final function init()
	{
		
		global $APPLICATION;
		$this->APP = &$APPLICATION;
		
		global $DB;
		$this->DB = &$DB;
		
		global $USER;
		$this->USER = &$USER;
		
		/** @todo Заказывать подключение модулей через реализацию абстракции */
		Loader::includeModule('iblock');
		
	}
	
	/**
	 * Обязательный метод.
	 *
	 * @return mixed
	 */
	abstract public function up();
	
	/**
	 * Обязательный метод.
	 *
	 * @return mixed
	 */
	abstract public function down();
	
}
