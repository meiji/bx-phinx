<?

use Symfony\Component\Console\Input\ArgvInput;
use Meiji\BxPhinx\Migration\Manager;


/**
 * Фасад для работы с миграциями Битрикса под phinx'ом
 *
 * @link   https://phinx.org/ PHP Database Migrations for Everyone
 * @link   https://github.com/cakephp/phinx Simple PHP Database Migrations
 *
 * @author Artem Iksanov <iksanov@meiji.media>
 */

/**
 * Предустановка констант для конфигурирования запуска ядра
 */
define('BX_BUFFER_USED', true); // Отключить буферизацию
define('NO_KEEP_STATISTIC', true); // Не учитывать запуск в логах
define('NOT_CHECK_PERMISSIONS', true); // Отключить контроль доступа
define('NO_AGENT_STATISTIC', true); // Отключить агентов
define('STOP_STATISTICS', true); // Отключить модуль статистики

/**
 * Запуск ядра
 */
include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php'); // Служебный пролог
\Bitrix\Main\Analytics\Counter::disable(); // Если в будущем откажутся от констант старого ядра
while (ob_get_level()) ob_end_flush(); // Сбрасываем буферы, если остались

/**
 * Обработка команды и параметров запуска
 */
$inputArgs = Manager::defineArgv($_SERVER['argv']);

/**
 * Определение серверных переменных
 */
$GLOBALS['DBType'] = 'mysql'; // Тип сервера БД

/** @var Phinx\Console\PhinxApplication $app */
$app = require __DIR__ . '/../../../robmorgan/phinx/app/phinx.php';

/**
 * Запуск Phinx
 */
$input = new ArgvInput($inputArgs);
$app->run($input);
