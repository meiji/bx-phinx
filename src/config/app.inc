<?php

use Bitrix\Main\Config\Configuration;
use CBXVirtualIo;


$migrationsDirectory = __DIR__ . '/../../../../../database/migrations';
$seedsDirectory      = __DIR__ . '/../../../../../database/seeds';

if (!CBXVirtualIo::GetInstance()->DirectoryExists($migrationsDirectory)) {
	CBXVirtualIo::GetInstance()->CreateDirectory($migrationsDirectory);
}

if (!CBXVirtualIo::GetInstance()->DirectoryExists($seedsDirectory)) {
	CBXVirtualIo::GetInstance()->CreateDirectory($seedsDirectory);
}

$bxConnectionsConfig = Configuration::getInstance()->get("connections");
$bxConnection        = $bxConnectionsConfig['default'];

return [
	'migration_base_class' => 'Meiji\BxPhinx\Migration\AbstractMigration',
	'paths'                => [
		'migrations' => realpath($migrationsDirectory),
		'seeds'      => realpath($seedsDirectory)
	],
	'environments'         => [
		'default_migration_table' => 'b_migration_log',
		'default_database'        => 'default',
		'default'                 => [
			'adapter' => 'mysql',
			'charset' => 'utf8',
			'host'    => $bxConnection['host'],
			'name'    => $bxConnection['database'],
			'user'    => $bxConnection['login'],
			'pass'    => $bxConnection['password']
		]
	]
];
